# LIBRARIES IMPORT

import numpy as np
import random
import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow import keras
from tensorflow.python.keras.layers.core import Activation

# DEFINE CALLBACK

class myCallback(tf.keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs={}):
    if(logs.get('acc')>0.85):
      print("\nReached 85% accuracy so canceling training!")
      self.model.stop_training = True

callbacks = myCallback()

# IMPORT AND LOAD MNIST FASHION DATA

mnist = keras.datasets.fashion_mnist
(training_images, training_labels), (test_images, test_labels) = mnist.load_data()

print('STEP 1: Data imported')

# DATA NORMALIZATION

# Original data a represented by grayscale values between 0 and 255.
# However, it's easier for the neural network to deal with values between 0 and 1.

training_images = training_images/255
test_images = test_images/255

print('STEP 2: Data normalized')

# DEFINE NEURAL NETWORK

model = keras.Sequential([keras.layers.Flatten(), \
                          keras.layers.Dense(512, activation = tf.nn.relu), \
                          keras.layers.Dense(10, activation = tf.nn.softmax)])

print('STEP 3: Neural network defined')

# COMPILE NEURAL NETWORK

model.compile(optimizer = tf.train.AdamOptimizer(), \
              loss = 'sparse_categorical_crossentropy', \
              metrics = ['accuracy'])

print('STEP 4: Neural network compiled')

# TRAIN MODEL

model.fit(training_images, training_labels, epochs = 5, verbose=1, callbacks=[myCallback()])

print('STEP 5: Neural network trained')

# EVALUATE MODEL ON TEST IMAGES

nn_eval = model.evaluate(test_images, test_labels, verbose=0)
print('STEP 6: Neural network evaluated on test images with accuracy of ' + str(np.round(nn_eval[1],4)*100) + '%')

# MODEL PREDICTIONS

# PLOT RANDOM TRAINING IMAGE

n = random.randint(0,len(test_images)+1)
plt.imshow(test_images[n])
plt.show()

classification = model.predict(test_images)
print('STEP 7: Neural network predictions generated')
print('    Random test:')
print('        Original image label ->                        ' + str(test_labels[n]))
print('        Image label predicted by neural network ->     ' + str(np.argmax(classification[n])))
