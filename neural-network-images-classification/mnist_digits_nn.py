# LIBRARIES IMPORT

import numpy as np
import random
import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow import keras

# SETTINGS

#    Accuracy threshold: will stop training the neural network once this level is reached
acc_lim = 98    # in %

# DEFINE CALLBACK

class myCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        if(logs.get('acc') > acc_lim / 100):
            print('\n\nTraining stopped after reaching ' + str(acc_lim) + '% accuracy')
            self.model.stop_training = True
      
callbacks = myCallback()

# IMPORT AND LOAD MNIST DIGITS DATA

mnist = keras.datasets.mnist
(training_set, training_labels), (test_set, test_labels) = mnist.load_data()

print('STEP 1: Data imported')

# DATA NORMALIZATION

# Original data a represented by grayscale values between 0 and 255.
# However, it's easier for the neural network to deal with values between 0 and 1.

training_set = training_set/255
test_set = test_set/255

print('STEP 2: Data normalized')

# DEFINE NEURAL NETWORK

model = keras.Sequential([keras.layers.Flatten(), \
                          keras.layers.Dense(1024, activation = 'relu'), \
                          keras.layers.Dense(10, activation = 'softmax')]) 

print('STEP 3: Neural network defined')

# COMPILE NEURAL NETWORK

model.compile(optimizer = 'adam', \
              loss = 'sparse_categorical_crossentropy', \
              metrics = ['accuracy'])

print('STEP 4: Neural network compiled')

# TRAIN MODEL

model.fit(training_set, training_labels, epochs = 1000, verbose=1, callbacks=[myCallback()])

print('STEP 5: Neural network trained')

# EVALUATE MODEL ON TEST SET

nn_eval = model.evaluate(test_set, test_labels, verbose=0)
print('STEP 6: Neural network evaluated on test images with accuracy of ' + str(np.round(nn_eval[1],4)*100) + '%')

# MODEL PREDICTIONS

# PLOT RANDOM TRAINING IMAGE

n = random.randint(0,len(test_set)+1)
plt.imshow(test_set[n])
plt.show(block=False)

classification = model.predict(test_set)
print('STEP 7: Neural network predictions generated')
print('    Random test:')
print('        Original digit label ->                  ' + str(test_labels[n]))
print('        Digit predicted by neural network ->     ' + str(np.argmax(classification[n])))
