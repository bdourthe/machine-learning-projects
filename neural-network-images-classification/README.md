# Machine learning projects (Python)

__
## Neural network images classification

#### Definition
This folder gathers a few tensorflow algorithms written to solve multiple image classification problems using regular and convolutional neural network

#### Content
    
| Code | Definition  |
| ---- |-------------|
| predict\_house\_price.py | Uses a basic single layer neural network to predict house pricing using a simple input training set |
| mnist\_digits\_nn.py     | Uses a multi-layers neural network (with callbacks) to identify patterns between different hand-written digits and creates a model able to predict the label of new non-labelled hand-written digits (based on the MNIST data set)|
| mnist\_fashion\_nn.py    | Uses a multi-layers neural network (with callbacks) to identify patterns between different clothing items and creates a model able to predict the label of new non-labelled items (based on the FASHION-MNIST data set)|
| emotions\_cnn.py         | Uses a multi-layers convolutional neural network (with callbacks) to identify patterns between different images of happy and sad people and creates a model able to predict the emotion (happy or sad) of a new portrait image (based on the Laurence-Moroney happy-or-sad data set)|
| mnist\_digits\_cnn.py    | Uses a multi-layers convolutional neural network (with callbacks) to identify patterns between different hand-written digits and creates a model able to predict the label of new non-labelled hand-written digits (based on the MNIST data set)|
| mnist\_fashion\_cnn.py   | Uses a multi-layers convolutional neural network (with callbacks) to identify patterns between different clothing items and creates a model able to predict the label of new non-labelled items (based on the FASHION-MNIST data set)|
| cats-v-dogs\_dcnn.py   | Uses a deep convolutional neural network (with data augmentation) to address the Cats v Dogs full Kaggle Challenge|
| horses-v-humans\_dcnn.py   | Uses transfer learning combined with a local deep convolutional neural network (with data augmentation) to enable a 99.9% classification accuracy using images of horses and humans|

#### Example

Output of the mnist\_fashion\_cnn.py code with a desired accuracy: level of 99%

![Alt text](neural-network-images-classification/img/fashion_mnist_example.jpg "Output sample")



