# LIBRARIES IMPORT

import numpy as np
import tensorflow as tf

from tensorflow import keras

# HELLO WORLD OF DEEP LEARNING

#    Define neural network
model = keras.Sequential([keras.layers.Dense(units=1, input_shape=[1])])
#    Compile neural network
model.compile(optimizer='sgd', loss='mean_squared_error')
#    Provide data
xs = np.array([-1, 0, 1, 2, 3, 4], dtype=float)
ys = np.array([-3, -1, 1, 3, 5, 7], dtype=float)
#    Train neural network
model.fit(xs, ys, epochs=500, verbose=0)
#    Print results
x = 10
print('HELLO WORLD OF DEEP LEARNING')
print('    True answer = ' + str(2*x-1))
print('    Model predicted answer = ' + str(np.round(np.double(model.predict([x])),2)))

# EXERCISE 1- HOUSE PRICES

#    Define neural network
model = keras.Sequential([keras.layers.Dense(units=1, input_shape=[1])])
#    Compile neural network
model.compile(optimizer='sgd', loss='mean_squared_error')
#    Provide data
xs = np.array([1, 2, 3, 4, 5, 6])
ys = np.array([1, 1.5, 2, 2.5, 3, 3.5])
#    Train neural network
model.fit(xs, ys, epochs=1000, verbose=0)
#    Print results
x = 7
print('EXERCISE 1- HOUSE PRICES')
print('    Real cost: ' + str(50*x+50) +'K')
print('    Estimated cost: ' + str(np.round(np.double(model.predict([x]))*100,2)) +'K')